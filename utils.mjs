import "dotenv/config"
import got from "got";
import { readdir } from "node:fs/promises"
import { pipeline as streamPipeline } from "node:stream/promises"
import { createWriteStream } from "node:fs"
import { extname, basename } from "node:path"

const query = `
query {
  findArticles(published: {is: true}) {
    name
    publicId
    people {
      name
      authorTitle
    }
    content {
      md
    }
    creationDate
    date
    description {
      text
    }
    seoTitle
    seoDescription {
      text
    }
    slug
    files {
      secret
      name
      contentType
    }
    hubs {
      name
    } 
    seriesOrPlaylist {
      name
    }
  }
}
`
export async function getFiberyArticles(fiberyAsCms, fiberyUrl, fiberyHeaders, fiberyArticleDatabase) {
  console.log(
    fiberyAsCms ? "fetched article from fibery" : "skipped fetching from fibery"
  )
  const { data, errors } = await got.post(fiberyUrl, {
    headers: fiberyHeaders,
    json: {
      query: query,
      },
    })
    .json()
  if (errors) {
    throw new Error(`Something went wrong: ${JSON.stringify(errors)}`)
  }
  if (!fiberyAsCms || !data) return [] 
  return data[`find${fiberyArticleDatabase}`] 
}

export function articleTemplate({
  content,
  title,
  description,
  seoTitle,
  seoDescription,
  date,
  author,
  avatar,
  authorTitle,
  category,
  tags,
  featuredImage = ``,
  socialImage = ``,
  series,
}) {
  return `---
title: "${title}"
description: "${description}"
date: ${JSON.stringify(date)}
author: ${JSON.stringify(author)}
avatar: ${avatar}
authorTitle: ${authorTitle}
category: ${category}
tags: ${JSON.stringify(tags)}
featuredImage: ${featuredImage}
socialImage: ${socialImage}
seoTitle: "${seoTitle}"
seoDescription: "${seoDescription}"
series: "${series}"
---
${content}
`
}

export async function validateArticles(articles, savedLocation) {
  const slugs = await getFileSourceSlugs(savedLocation)
  const slugsMap = new Map([])
  for (const article of articles) {
    if (!article.slug) {
      throw new Error(
        `Article ${article.name} with publicId ${article.name} does not have a slug. This article will not be created. Please add a slug to the article in Fibery`
      )
    }
    if (slugs.has(article.slug)) {
      throw new Error(
        `Duplicate ${article.name} with publicId ${article.name} and slug ${article.slug}. This article already exists in the blog file system.`
      )
    }
    if (slugsMap.has(article.slug)) {
      const origin = slugsMap.get(article.slug)
      throw new Error(
        `Duplicate ${article.name} with publicId ${article.name} and slug ${article.slug}. This article already exists in Fibery. Origin: ${origin.name} with publicId ${origin.publicId}. Please remove the duplicate in Fibery.`
      )
    }
    slugsMap.set(article.slug, {
      publicId: article.publicId,
      name: article.name,
    })
  }
}

export async function loadAvatar(author, fiberyHost, fiberyHeaders) {
  let avatars = author?.avatars
  if (avatars?.length) {
    const avatar = avatars[0]
    const secret = avatar.secret
    const name = avatar.name
    const ext = extname(name)
    const fileName = secret + ext
    const cached = avatarsCache.get(secret)
    if (cached) {
      return fileName
    }
    await downloadImage(`/api/files/${secret}`, assetsDir + "/" + fileName, fiberyHost, fiberyHeaders)
    avatarsCache.set(secret, fileName)
    return fileName
  }
  return `fiberylogo.png`
}

export function downloadImage(url, path, fiberyHost, fiberyHeaders) {
  console.log(got.stream(fiberyHost + url, { headers: fiberyHeaders }))
  return streamPipeline(
    got.stream(fiberyHost + url, { headers: fiberyHeaders }),
    createWriteStream(path)
    )
  }

export async function getFileSourceSlugs(savedLocation) {
  return new Set(
    (await readdir(savedLocation, { withFileTypes: true }))
      .filter((dirent) => dirent.isDirectory())
      .map((dirent) => dirent.name)
  )
}

export async function downloadFeaturedAndSocialImage(files, articleDir, fiberyHost, fiberyHeaders) {
  let featuredImage
  let socialImage
  for (const { name, secret } of files) {
    const ext = extname(name)
    const nameWithoutExt = basename(name, ext)
    if (nameWithoutExt === "featuredImage" && !featuredImage) {
      featuredImage = secret + ext
      await downloadImage(
        `/api/files/${secret}`,
        articleDir + "/" + featuredImage, 
        fiberyHost, 
        fiberyHeaders
      )
    }
    if (nameWithoutExt === "socialImage" && !socialImage) {
      socialImage = secret + ext
      await downloadImage(
        `/api/files/${secret}`,
        articleDir + "/" + featuredImage, 
        fiberyHost, 
        fiberyHeaders
      )
    }
  }
  if (!featuredImage && files[0]) {
    const { name, secret } = files[0]
    const ext = extname(name)
    featuredImage = secret + ext
    await downloadImage(
      `/api/files/${secret}`,
      articleDir + "/" + featuredImage, 
      fiberyHost, 
      fiberyHeaders
    )
  }
  return {
    featuredImage: featuredImage || socialImage,
    socialImage: socialImage || featuredImage,
  }
}

export function convertRelationFieldDataToArray(fieldData){
  let results = [] 
  for (const item of fieldData){
    results.push(item.name)
  } 
  return results.join() 
} 

// const fiberyToken           = process.env["FIBERY_TOKEN"]
// const fiberyHost            = process.env["FIBERY_HOST"]
// const fiberyArticleSpace    = process.env["FIBERY_ARTICLE_SPACE"]
// const fiberyArticleDatabase = process.env["FIBERY_ARTICLE_DATABASE"]

// const fiberyUrl = `${fiberyHost}/api/graphql/space/${fiberyArticleSpace}`
// const fiberyHeaders = {
//   Authorization: `Token ${fiberyToken}`,
// }
// const articles = await getFiberyArticles(true, fiberyUrl, fiberyHeaders, fiberyArticleDatabase) 
// console.log("🚀 ~ articles:", articles)
// articles.forEach(article => {
//   console.log("🚀 ~ article.contacts:", article.contacts)
//   const author = convertRelationFieldDataToArray(article.contacts)
//   console.log(author)
// } ) 
// console.log(JSON.stringify(articles, null, 2))
// debugger