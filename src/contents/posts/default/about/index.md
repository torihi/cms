---
title: Introduce About Writer
description: Đôi lời giới thiệu về bản thân mình
date: "2024-04-24"
author: Kendy Đỗ
tags:
  - Introduce
---

> 💡Tôn trọng sự khác biệt 

## 1. Về bản thân mình🧸

### Danh xưng nào cho phù hợp

- Mình là Đỗ Hàng Minh Trí, cứ gọi là Kendy cho thân quen, vì cha sinh mẹ đẻ, cái tên đó đã được ấn định cho mình. 

- Mình sẽ không liệt kê những thứ như 1 lời introduce bản thân theo khuôn mẫu như 1 JD hay CV, hoặc là liệt kê điểm mạnh điểm yếu mà mọi người hay gọi là Ikigai, SWOT của bản thân mình vì mình không muốn tự tù, tự giới hạn, tự đóng khung bản thân mình vào 1 keyword nào
  - Việc không bám víu, không phụ thuộc vào 1 nhân tố nào khiến mình dễ mở lòng trong việc đón nhận sự khác biệt từ mọi thứ bên ngoài, đồng thời cũng khiến mình dễ cập nhật, thích ứng với các thay đổi cần được đổi thay. 

---

## 2. Về trang web dohangminhtri.com ⚙️

Mình cần 1 thói quen để duy trì việc **re-think** x2 (nghĩ ⇄ 1 lần suy nghĩ, viết ⇄ 1 lần suy nghĩ), đây là 1 cách tốt để mình luyện cơ não + tránh các bệnh về lý thuyết, hay lún đầm vào việc hoá thân thành mọt sách. 
- Và viết dù là dumping hay journaling hay chiết xuất highlight, hoặc là tạo evergreen, cũng khiến bạn truy xuất 1 cấu thấu thiền và đầy minh triết lại các thước phim đã chạy qua

> **Mình quan niệm việc học ko có điểm kết**
>
> - Cần gì học đó, nên việc học gần như là sự cần của cả đời

Kiến thức gần như đều là thứ mà 1 cá nhân góp nhặt, nên gần như nó không thuộc về quyền sở hữu của ai hết. Và mình nhận ra mình không thể nhớ tất cả, nên việc hệ thống hoá và đóng gói kiến thức với 1 nơi không phải não bộ vật lý khiến mình dễ thở và không phải quá tải hơn trong việc lưu trữ
- 💢 1 vấn đề mình nhận ra dù là kiến thức đã được tiêu thụ, được hấp thụ, được tiến hành chiết xuất từ thông tin ban đầu, cho dù nó là hệ quả của 1 hệ thống nào đi chăng nữa như Bloom Taxonomy, Zettelkasten … thì cái kết quả đó vẫn chỉ ở mức Lưu trữ (Storage) ⇄ chỉ dừng ở mức độ diễn giải cho sự học
- 💡 Nên ý tưởng sẽ là công khai các dữ liệu ở dạng tù này, giải phóng những gì mình học như 1 dạng Learning in public
  - mình biết 1 điều là những kiến thức, trải nghiệm, kinh nghiệm của mình sẽ ko bao giờ đúng khi áp dụng và thực thể hay hoàn cảnh, điều kiện khác. Cũng chính bởi điều này nên mình ko bao giờ truy xuất kinh nghiệm cá nhân để dẫn hướng, hay đưa ra lời khuyên cho bất kì ai
  - Mọi thứ chỉ tương đối như einstein nói, hãy trung đạo như phật nói, và cái gì quá cũng ko tốt như má mình nói 
    - Hãy xem những thứ bạn đang đọc ở đây như 1 các mảnh rác trong kho rác SEO của google, lọc những mảnh rác nào ít dơ nhất và có thể dùng được
      - Kiến thức chỉ là công cụ tốt, chỉ khi nó được dùng (từ tầng 3 Bloom taxonomy trở lên). Đừng để nó bị chôn vùi, hay bị nghiện việc học, hoặc tệ hơn là béo phì tri thức. Nó ko phải thứ để tàng thu hãy liễm hạnh, 
        - Hãy chuyển hoá raw data, thậm chí processed data (atomic notes, evergreen notes ..) hay bất cứ từ mĩ miều nào kiến thức, tri thức thành những thứ phục vụ cho nhu cầu cá nhân của bạn và như câu nói nửa đùa, nửa thật 
> **Giỏi để làm gì, khi không giàu**

Ngoài mục đích chia sẻ những gì mình học cho mọi người, mình cũng mong Parid, Trinity, Gusta … Các con có thể tham vấn được hay học được 1 giá trị nào đó trong lộ trình các bài viết của papa Kendy 
- Rằng pa không cố xây dựng papa Kendy thành 1 mẫu người hoàn hảo; pa là 1 con người hết sức bình thường, và pa cũng có các vấn đề như bao người khác. Pa cần học, cần thích ứng và cần đối mặt với mọi thứ đến với đời pa
  - Cái gì đến thì mình tiếp. Và papa kendy vui khi có tụi con đồng hành cùng pa mẹ  
> Papa Kendy lúc nào cũng tự hào về con, và thương con trên hết mọi sự 💚 

---

## 3. Ủng hộ 🍩

Phần này mình không rõ có nên thêm vào hay không, nhưng mình nghĩ 1 vài người tử tế đôi khi chỉ muốn mời mình 1 ly nước, vậy cũng không hẳn là vì cái gì to tát mà khước từ, nhưng hãy để lại danh tính của bạn để mình còn biết hậu đãi lại lần tới như 1 mutal partner nên làm ha 
- Momo: https://me.momo.vn/9vIaT5sOFqiwUWIBUxTJtM
- Vietcombank: 9372797979 Đỗ Hàng Minh Trí

---

## 4. Liên hệ 📞

Mình có để các nền tảng các bạn có thể liên hệ mình ở dưới ảnh đại diện. Đừng ngần ngại làm phiền mình nếu bạn cảm thấy cần, mình không chắc nguồn lực của mình đủ để có giải pháp hay đủ chuyên môn để giúp mọi vấn đề. Nhưng bạn sẽ có 1 người lắng nghe bạn như 1 quan toà 🙂

> ⚠️Disclaim: Các nguồn lực mình có, những thứ mình thể hiện qua từng bài viết, chỉ là những thứ mình nạp vào tiêu thụ + đồng thời tự sản xuất ra dựa trên quá trình vận hành với thế giới thực tại của mình, nên nếu có gì có thể phù hợp với nguồn lực của bạn, hãy dùng. Nếu bạn cần nâng cao, hãy làm phiền mình qua cái đường dẫn liên hệ bên trên. Mình sẽ sắp xếp time-block và phản tư với bạn
>
> - Việc bạn vẫn còn neo trong trang này, thì ắt hẳn bạn cũng phải có 1 mối bận tâm nào cần san sẻ mà chưa có đáp án. Mình không hành nghề life coaching hay mentoring hoặc thậm chí thầy bà nào trên vị thế của bạn. Nhưng thứ mình cho được, mình sẽ cho mà ko kì vọng hoàn trả; nó không phải là 1 giao dịch
