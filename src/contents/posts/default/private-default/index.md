---
title: Default Private
date: "2024-04-24"
tags:
  - private-tag
isPrivate: true
previewImage: banned.png
author: Kendy Đỗ
---

## Default Private Posts

This is a default private post. It's recommended not to delete this posts 😵
