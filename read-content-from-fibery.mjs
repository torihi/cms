
import got from "got"
import process from "node:process"
import { extname, basename } from "node:path"
import unified from "unified"
import remarkParse from "remark-parse"
import visit from "unist-util-visit"
import { mkdir, writeFile, readdir } from "node:fs/promises"
import toMarkdown from "mdast-util-to-markdown"
import { getFiberyArticles, articleTemplate, loadAvatar, validateArticles, downloadImage, downloadFeaturedAndSocialImage, convertRelationFieldDataToArray} from "./utils.mjs";

const fiberyAsCms           = process.env["FIBERY_AS_CMS"]
const fiberyHost            = process.env["FIBERY_HOST"]
const fiberyToken           = process.env["FIBERY_TOKEN"]
const fiberyArticleDatabase = process.env["FIBERY_ARTICLE_DATABASE"]
const fiberyArticleSpace    = process.env["FIBERY_ARTICLE_SPACE"]
const savedLocation         = process.env["SAVE_LOCATION"]
const assetsDir             = process.env['ASSET_DIR']

if (!fiberyHost) {
  throw new Error("FIBERY_HOST is not set")
}
if (!fiberyToken) {
  throw new Error("FIBERY_TOKEN is not set")
}
if (!fiberyArticleDatabase) {
  throw new Error("FIBERY_ARTICLE_DATABASE is not set")
}
if (!fiberyArticleSpace) {
  throw new Error("FIBERY_ARTICLE_SPACE is not set")
}

const fiberyUrl = `${fiberyHost}/api/graphql/space/${fiberyArticleSpace}`
const fiberyHeaders = {
  Authorization: `Token ${fiberyToken}`,
}
let label = "read content from fibery"
console.time(label)
const avatarsCache = new Map()
const articles = await getFiberyArticles(fiberyAsCms, fiberyUrl, fiberyHeaders, fiberyArticleDatabase) 
await validateArticles(articles, savedLocation)

for (const article of articles) {
  // if (article.name !=='Bloom Taxonomy') continue
  // if (article.publicId !==7) continue
  console.log("🚀 ~ article:", article)
  const { md } = article.content
  if (!article.slug) {
    continue
  }
  const articleDir = `${savedLocation}/${article.slug}`
  try {
    await mkdir(articleDir, { recursive: true })
  } catch (err) {
    console.error(err)
  }
  const contentAST = unified().use(remarkParse).parse(md)
  const files = []
  visit(contentAST, "image", async (node) => {
    const url = node.url
    if (url.startsWith("/api/files/")) {
      const urlObject = new URL(url, fiberyHost)
      const ext = extname(node.alt)
      const imageName = urlObject.pathname.replace("/api/files/", "") + ext
      files.push({ url, name: imageName })
      node.url = "./" + imageName
      node.alt = node.title ? node.title : ""
    }
  })
  visit(contentAST, "link", async (node) => {
    const url = node.url
    if (url.startsWith("https://") && url.endsWith(".mp4")) {
      node.type = `html`
      node.value = `<video autoPlay controls loop muted playsInline style={{width: "100%", maxWidth: 700}}>
    <source src="${node.url}" type="video/mp4" />
</video>`
    }
  })
  for (const file of files) {
    await downloadImage(file.url, articleDir + "/" + file.name, fiberyHost, fiberyHeaders)
  }
  const { featuredImage, socialImage } = await downloadFeaturedAndSocialImage(article.files, articleDir, fiberyHost, fiberyHeaders)
  await writeFile(
    articleDir + "/index.md",
    articleTemplate({
      content: toMarkdown(contentAST, { resourceLink: true }),
      title: article.name,
      tags: article.hubs ? article.hubs.map(({ name }) => name) : [],
      author: convertRelationFieldDataToArray(article.people) || "Kendy Đỗ",
      authorTitle: article.people?.authorTitle || "Weirdo",
      avatar: await loadAvatar(article.people, fiberyHost, fiberyHeaders),
      category: article.seriesOrPlaylist?.name || "Uncategorized",
      description: article.description.text.replaceAll("\n", " "),
      seoDescription: article.seoDescription.text.replaceAll("\n", " ").substring(0, 160),
      seoTitle: article.seoTitle ?? "",
      date: article.date || article.creationDate,
      featuredImage,
      socialImage,
      series: article.seriesOrPlaylist.name
    })
  )
}
console.timeEnd(label)